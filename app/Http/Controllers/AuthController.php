<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $request){
        //dd($request->all());
        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];
        $namalengkap = ucwords(strtolower($namadepan)." ".strtolower($namabelakang));
        $gender = $request['gender'];
        $warganegara = $request['warganegara'];
        $bahasa = $request['bahasa'];
        $bio = $request['bio'];
        return view('welcome',compact('namalengkap'));
    }
}
